<?php

/*
Precargar las clases
 */
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección</h1>

        <p class="lead">Realizando algunas consultas de selección sobre las tablas emple y depart.</p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-2">
                <h2>Listar todos los empleados</h2>

                <p><?= Html::a("Ejecutar consulta", ["emple/consulta"], ["class"=>" btn btn-default"]) ?></p>

            </div>
            
            <div class="col-lg-2">
                <h2>Listar todos los empleados</h2>

                <p><?= Html::a("Ejecutar consulta", ["emple/consulta"], ["class"=>" btn btn-default"]) ?></p>

            </div>
            
            <div class="col-lg-2">
                <h2>Listar todos los empleados</h2>

                <p><?= Html::a("Ejecutar consulta", ["emple/consulta"], ["class"=>" btn btn-default"]) ?></p>

            </div>
            
            <div class="col-lg-2">
                <h2>Listar todos los empleados</h2>

                <p><?= Html::a("Ejecutar consulta", ["emple/consulta"], ["class"=>" btn btn-default"]) ?></p>

            </div>
            
            <div class="col-lg-2">
                <h2>Listar todos los empleados</h2>

                <p><?= Html::a("Ejecutar consulta", ["emple/consulta"], ["class"=>" btn btn-default"]) ?></p>

            </div>
            
            <div class="col-lg-2">
                <h2>Listar todos los empleados</h2>

                <p><?= Html::a("Ejecutar consulta", ["emple/consulta"], ["class"=>" btn btn-default"]) ?></p>

            </div>
        </div>

    </div>
</div>
